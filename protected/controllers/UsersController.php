<?php

class UsersController extends Controller {
        public function actions() {
        // captcha action renders the CAPTCHA image displayed on the register page
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'foreColor' => 0xfb8112,
                'offset' => 5,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        $model = new Users;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-index-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                if ($model->save()) {
                    $this->redirect(array('site/login'));
                }
                return;
            }
        }
        $this->render('index', array('model' => $model));
    }
    public function beforeAction($action) {
        if( parent::beforeAction($action) ) {
            /* @var $cs CClientScript */
            $cs = Yii::app()->clientScript;
            /* @var $theme CTheme */
            $theme = Yii::app()->theme;
            $cs->registerPackage('jquery');
            $cs->registerPackage('history');
            $cs->registerPackage('jquery-cookie');
            $cs->registerPackage('jquery-easing');
            $cs->registerPackage('soundcloud');
            $cs->registerScriptFile( Yii::app()->request->baseUrl.'/javascripts/main.js');
//                $cs->registerCssFile($theme->getBaseUrl() . '/css/main.css');
            return true;
        }
        return false;
    }
}