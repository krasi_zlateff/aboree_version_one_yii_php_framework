<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

    $this->layout = 'modal';
    $this->pageTitle = Yii::app()->name . ' - Login';
    $this->breadcrumbs = array(
        'Login',
    );
    
    $this->pageclass = 'modal-page login-page';
    $this->maintitle = 'login to aboree';
    $this->subtitle = 'enjoy your party night';
    $this->headimage = '/storage/images/backgrounds/5a.jpg';
?>
<p>Please fill out the following form with your login credentials:</p>
<div class="row no-margin">
    <div class="form-block">
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions'=>array(
                    'class'=>'form',
                ),
            ));
        ?>
        <p class="note">Fields below are required *</p>
        <div class="input-field">
            <?php echo $form->textField($model,'username', array( 'placeholder'=>'username', 'class'=>'input username')); ?>
            <?php echo $form->error($model,'username'); ?>
            <a class="fg urnm" href="#">forgot your username?</a>
        </div>
        <div class="input-field">
            <?php echo $form->passwordField($model,'password', array('placeholder'=>'password', 'class'=>'input password')); ?>
            <?php echo $form->error($model,'password'); ?>
            <a class="fg psd" href="#">forgot your password?</a>
        </div>
        <div class="clearfix"></div>
        <p class="hint">
            <b><i>Hint:</i></b> You can try aboree with demo account. login with <kbd>demo</kbd>/<kbd>demo</kbd>
        </p>
        <div class="rememberMe">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </div>
        <?php echo CHtml::submitButton('Login', array('class'=>'login-button')); ?>
        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="row no-margin">
    <div class="login-separator">
        <span>or</span>
    </div>
</div>
<div class="row no-margin">
    <div class="social-media-login">
        <div class="facebook">
            <span class="icon"></span>
            <span class="social-media-login-text">facebook connect</span>
        </div>
        <div class="twitter">
            <span class="icon"></span>
            <span class="social-media-login-text">sign in with twitter</span>
        </div>
    </div>
</div>

<div class="agreement">
    If you use "facebook connect" or "sign in with twitter" and are not an aboree user,<br /> 
    you will be registered and you agree to aboree's 
    <a href="<?php echo $this->createUrl('site/page',array('view'=>'privacy'))?>"><u>privacy policy</u></a>.
</div>
<div class="form-block-footer">
    Not a member yet? <a href="<?php echo $this->createUrl('users/index')?>">Register now</a> - it's free and easy!
</div>           
