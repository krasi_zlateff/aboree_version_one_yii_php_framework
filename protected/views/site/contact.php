<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

    $this->layout = 'modal';
    $this->pageTitle=Yii::app()->name . ' - Contact Us';
    $this->breadcrumbs=array(
            'Contact',
    );
    $this->pageclass = 'modal-page feedback-page';
    $this->maintitle = 'get in touch';
    $this->subtitle = 'because your opinion matters!';
    $this->headimage = '/storage/images/backgrounds/4a.jpg';
?>
<?php if(Yii::app()->user->hasFlash('contact')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('contact'); ?>
    </div>
    <?php else: ?>
    <p>
        If you have business inquiries or other questions, 
        please fill out the following form to contact us.
    </p>
    <div class="form-block">
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions'=>array(
                    'class'=>'form',
                ),
            ));
        ?>
        <p class="note">Fields below are required *</p>
        <?php echo $form->errorSummary($model); ?>
        <div class="row no-margin">
            <div class="input-field">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name', array( 'placeholder'=>'e.g. John Smith', 'class'=>'input username')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="input-field">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email', array( 'placeholder'=>'e.g. john.smith@aboree.com', 'class'=>'input email')); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
        </div>
        <div class="row no-margin">
            <div class="input-field">
                <?php echo $form->labelEx($model,'subject'); ?>
                <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128, 'placeholder'=>'You guys, are awesome.', 'class'=>'input subject')); ?>
                <?php echo $form->error($model, 'subject'); ?>
            </div>
        </div>
        <div class="row no-margin">
            <div class="input-field">
                <?php echo $form->labelEx($model,'body'); ?>
                <?php echo $form->textArea($model, 'body', array('rows' => 6, 'cols' => 50, 'class'=>'textarea', 'placeholder'=>'aboree is my new favourite web application.')); ?>
                <?php echo $form->error($model, 'body'); ?>
            </div>
        </div>
        <?php if (CCaptcha::checkRequirements()): ?>
        <div class="row no-margin">
            <div class="captcha-form">
                <div class="row no-margin">
                    <?php echo $form->labelEx($model, 'verifyCode'); ?>
                   <?php $this->widget('CCaptcha'); ?>
                    <div class="input-field">
                        <?php echo $form->textField($model, 'verifyCode', array('class'=>'input captcha-field')); ?>
                    </div> 
                </div>
                <div class="note">Please enter the letters as they are shown in the image above.
                    <br/>Letters are not case-sensitive.
                </div>
                <?php echo $form->error($model, 'verifyCode'); ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="row no-margin buttons">
            <?php echo CHtml::resetButton('start over', array('class'=>'reset-button')); ?>
            <?php echo CHtml::submitButton('Submit', array('class'=>'button')); ?>
        </div>
        <?php $this->endWidget(); ?>
    <!-- form -->
    </div>
<?php endif; ?>


