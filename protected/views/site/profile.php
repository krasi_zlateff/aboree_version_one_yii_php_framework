<?php
/* @var $this SiteController */

    $this->layout='profile';
    $this->pageTitle='hello '. Yii::app()->user->name;
    $this->breadcrumbs=array(
            'about',
    );
    $this->pageclass= 'profile-page';
?>
<div class="profile-content-wrapper">
    <div class="profile-content">
        <div class="clearfix"></div>
        <div class="main-cotrols">
            <div class="control-button check-mates">check your mates</div>
            <div class="control-button last-night">#?%! last night</div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>