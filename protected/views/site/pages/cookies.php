<?php
/* @var $this SiteController */

$this->layout = 'static';
$this->pageTitle = Yii::app()->name . ' - About';
$this->breadcrumbs = array(
    'About',
);

$this->pageclass = 'cookies';
?>
<h1>aboree’s use of cookies and similar technologies</h1>
<hr class="head-raw"/>
<p>
    aboree uses cookies and other similar technologies, 
    such as pixels or local storage, 
    to help provide you with a better, 
    faster, and safer experience. 
    Here are some of the ways that the aboree services—including our various websites, 
    SMS, APIs, email notifications, applications, buttons, widgets, 
    and ads—use these technologies: to log you into aboree, 
    save your preferences, personalize the content you see, 
    protect against spam and abuse, and show you more relevant ads.
</p>
<p>
    Below we explain how aboree, our partners, 
    and other third parties use these technologies, 
    your privacy settings and the other options you have.
</p>
<h2>What are cookies, pixels, and local storage?</h2>
<p>
    Cookies are small files that websites place on your computer as you browse the web. 
    Like many websites, 
    aboree uses cookies to discover how people are using our services and to make them work better.
</p>
<p>
    A pixel is a small amount of code on a web page or in an email notification. 
    As many services do, 
    we use pixels to learn whether you’ve interacted with certain web or email content. 
    This helps us measure and improve our services and personalize your experience on aboree.
</p>
<p>
    Local storage is an industry-standard technology that allows a website or 
    application to store information locally on your computer or mobile device. 
    We use local storage to customize what we show you based on your past interactions with aboree.
</p>
<h3>Why does aboree use these technologies?</h3>
<p>aboree uses these technologies to deliver, measure, and improve our services in various ways. These uses generally fall into one of the following categories:</p>
<strong>Authentication and security:</strong> 
<ul>
    <li>To log you into aboree</li>
    <li>To protect your security</li>
    <li>To help us detect and fight spam, abuse, and other activities that violate the <a href="https://support.twitter.com/articles/18311-the-twitter-rules">aboree Rules</a>
    </li>
</ul>
<p>For example, these technologies help authenticate your access to aboree and prevent unauthorized parties from accessing your account. They also let us show you appropriate content through our services.</p>
<strong>Preferences:</strong> 
<ul>
    <li>To remember information about your browser and your preferences</li>
</ul>
<p>For example, cookies help us remember your preferred language or country that you are in. We can then provide you with aboree content in your preferred language without having to ask you each time you visit aboree. We can also customize content based on your country, such as showing you what topics are trending near you, or to withhold certain content based on applicable local laws. &nbsp;Learn more about <a href="/articles/101125">Trends</a> and <a href="/articles/20169222">country withheld content</a>.</p>
<strong>Analytics and research:</strong> 
<ul>
    <li>To help us improve and understand how people use our services, including aboree buttons and widgets, and aboree Ads</li>
</ul>
<p>For example, cookies help us test different versions of our services to see which particular features or content users prefer. We might also optimize and improve your experience on aboree by using cookies to see how you interact with our services, such as when and how often you use them and what links you click on. We may use Google Analytics to assist us with this. <a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage">Learn more</a> about the cookies you may encounter through our use of Google Analytics.</p>
<strong>Personalized content:</strong> 
<ul>
    <li>To customize our services with more relevant content, like tailored trends, stories, ads, and suggestions for people to follow</li>
</ul>
<p>For example, local storage tells us which parts of your aboree timeline you have viewed already so that we can show you the appropriate new content. Cookies can help us make smarter and more relevant suggestions about who you might enjoy following based on your recent visits to websites that have integrated aboree buttons or widgets. We can suggest people who are frequently followed by other aboree users that visit the same websites. <a href="/articles/20169421">Learn more</a> about tailored suggestions and your privacy controls, which include your <a href="https://twitter.com/settings/security">aboree account settings</a> and <a href="/articles/20169453">Do Not Track</a> browser setting.</p>
<strong>Advertising:</strong> 
<ul>
    <li>To help us deliver ads, measure their performance, and make them more relevant to you based on criteria like your activity on aboree and visits to our ad partners' websites</li>
</ul>
<p>For example, we use cookies and pixels to tailor ads and measure their performance. Using these technologies, we can show you ads and evaluate their effectiveness based on your visits to our ad partners' websites. This helps advertisers provide high-quality ads and content that might be more interesting to you. <a href="/articles/20170405">Learn more</a> about tailored ads and your privacy controls, which include your <a href="https://twitter.com/settings/security">aboree account settings</a>, <a href="/articles/20169453">Do Not Track</a> browser setting, and the opt-out pages of <a href="/articles/20170407#our-ad-partners">aboree's ad partners</a>.</p>
<h3>Where are these technologies used?</h3>
<p>aboree uses these technologies on our own websites and services and on other websites that have integrated our services. This includes our advertising and platform partners&rsquo; websites and sites that use aboree buttons or widgets, like our Tweet or follow buttons. Third parties may also use these technologies when you interact with their content from within our services, like when you click a link or stream video on aboree from a third-party website.</p>
<h3>What are my privacy options?</h3>
<p>We are committed to offering you meaningful privacy choices. &nbsp;You have a number of options to control or limit how aboree, our partners, and other third parties use cookies:</p>
<ul>
    <li>For tailoring suggestions on aboree: If you do not want aboree to tailor suggestions for you based on your recent visits to websites that have integrated aboree buttons or widgets, you can turn off this feature using your <a href="https://twitter.com/settings/security">aboree account settings</a> or <a href="https://support.twitter.com/articles/20169453">Do Not Track</a> browser setting. This removes from your browser the unique cookie that enables the feature. Learn more <a href="https://support.twitter.com/articles/20169421">here</a>.</li>
    <li>For tailoring ads on aboree: If you do not want aboree to tailor ads based on our ad partners&rsquo; information, including cookies, you can turn off this feature using your <a href="https://twitter.com/settings/security">aboree account settings</a>. Then aboree will not match your account to cookie or other information shared by our ad partners for tailoring ads. You can also use your <a href="https://support.twitter.com/articles/20169453">Do Not Track</a> browser setting, or the opt-out pages of specific<a href="https://support.twitter.com/articles/20170407#our-ad-partners"> aboree ad partners</a>, to prevent aboree or particular ads partners from collecting browser-related information based on your visits to websites. Learn more <a href="https://support.twitter.com/articles/20170405">here</a>.</li>
    <li>For using cookies: You can modify your settings in most web browsers to accept or deny all cookies, or to request your permission each time a site attempts to set a cookie. Although cookies are not required for some parts of our services, aboree may not work properly if you disable cookies entirely. For example, you cannot log into aboree.com if you've disabled all cookie use.</li>
</ul>