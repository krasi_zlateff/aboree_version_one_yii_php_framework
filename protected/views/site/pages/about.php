<?php
/* @var $this SiteController */

$this->layout='static';
$this->pageTitle=Yii::app()->name . ' - about';
$this->breadcrumbs=array(
	'about',
);

$this->pageclass= 'about';
?>
<h1>Who are we and what is aboree?</h1>
<hr class="head-raw"/>
<div class="youtube-video">
    <iframe src="https://player.vimeo.com/video/49337552" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/49337552">DT 289: Dubfire @ Solar Dance Arena (Burgas - Bulgaria)</a> from <a href="https://vimeo.com/dancetrippintv">DanceTrippin</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
</div>
<p>We’re glad you asked.</p>
<p>We help you easily discover closest parties.
    Tell us what you like to drink, what music you like to listen, and we’ll introduce you to amazing parties of your friends.</p>
<p>As you Stumble through great web pages, 
    tell us whether you Like or Dislike our recommendations so we can show you more of what’s best for you. 
    We’ll show you web pages based on that feedback as well as what similar Stumblers and the people you follow have Liked or Disliked.</p>
<p>Our members have given us some pretty great compliments in the past, 
    including describing us as “the entire Internet, all in one place,”  
    ”an epic journey” and “a map to an adventure you wouldn’t otherwise have found out about.”</p>
<p>Whether you’re interested in Humor, 
    Photography, Fashion or Sports, 
    we have something for you.  
    Every Stumble is an adventure, and something amazing is always just a click away.</p>

<a href="<?php echo $this->createUrl('site/login'); ?>" class="sign-up button">Get started</a>