<?php
/* @var $this SiteController */

    $this->layout='modal';
    $this->pageTitle=Yii::app()->name . ' - register';
    $this->breadcrumbs=array(
            'register',
    );

    $this->pageclass= 'modal-page register-page';
    $this->maintitle = 'open premium account';
    $this->subtitle = 'party night starts over';
?>
<p>Please fill out the following form with your login credentials:</p>
<div class="row no-margin">
    <div class="form-block">
        <p class="note">Fields below are required *</p>
        <div class="form">
            <div class="row no-margin">
                <div class="input-field">
                    <input type="text" class="input username" placeholder="username"  disabled="disabled">
                </div>
                <div class="input-field">
                    <input type="text" class="input password" placeholder="password"  disabled="disabled">
                </div>
                <div class="input-field">
                    <input type="text" class="input email" placeholder="email"  disabled="disabled">
                </div>
            </div>
            <div class="row no-margin">
                <div class="date-of-birth">
                    <span class="dob">date of birth</span>
                    <select class="month" disabled="disabled">
                        <option>month</option>
                    </select>
                    <select class="day" disabled="disabled">
                        <option>day</option>
                    </select>
                    <select class="year" disabled="disabled">
                        <option>year</option>
                    </select>
                </div>
            </div>
            <div class="gender">
                <span class="sex">Gender</span>
                <div class="male">
                    <input type="radio" id="male" disabled="disabled"/>
                    <label for="male">male</label>
                </div>
                <div class="female">
                    <input type="radio" id="female" disabled="disabled"/>
                    <label for=fe"male">female</label>
                </div>
            </div>
            <a href="#" class="sign-up button">open account</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row no-margin">
    <div class="closed-registrations">New registrations are closed!</div>
</div>
<div class="row no-margin">
    <div class="login-separator">
        <span>or sign in through</span>
    </div>
</div>
<div class="row no-margin">
    <div class="social-media-login">
        <div class="soundcloud">
            <span class="icon"></span>
            <span class="social-media-login-text">SoundCloud</span>
        </div>
        <div class="facebook">
            <span class="icon"></span>
            <span class="social-media-login-text">facebook</span>
        </div>
        <div class="twitter">
            <span class="icon"></span>
            <span class="social-media-login-text">twitter</span>
        </div>
    </div>
</div>

<div class="agreement">
    If you use "SoundCloud" or "facebook" or "twitter" for opening new premium account and are not an aboree user,<br /> 
    you will be registered and you agree to aboree's 
    <a href="<?php echo $this->createUrl('site/page',array('view'=>'privacy'))?>"><u>privacy policy</u></a>.
</div>
<div class="form-block-footer">
    Already have an account? <a href="<?php echo $this->createUrl('site/login')?>">Login</a> here.
</div>