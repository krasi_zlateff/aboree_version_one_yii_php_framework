<?php
/* @var $this SiteController */

$this->layout='static';
$this->pageTitle=Yii::app()->name . ' - careers';
$this->breadcrumbs=array(
	'careers',
);

$this->pageclass= 'carrers';

?>
<div class="head">
    <h1>Build your career at aboree</h1>
    <h4>Come join us, it’s fun here.</h4>
</div>
<hr />
<div class="job-position">
    <div class="job-title">
        <h2>Software Engineer</h2>
        <h3>Sofia, Bulgaria</h3>
    </div>
    <div class="job-description">
        <p>
            aboree is looking to hire a software developer who can help us build a platform 
            that brings people closer together by allowing them to have great video conversations over the Internet.
        </p>
        <p>
            The ideal candidate is self-motivated, a fast learner and passionate about building good software. He or she has a strong grasp of computer science fundamentals, experience working across different technologies, and the ability to think critically about problems and choose the right course of action.
        </p>
        <h3>What you will be doing?</h3>
        <p>From day one, you will be writing software to help the aboree team build a video conversation product that could revolutionize the way people communicate across the Internet.</p>
        <p>A passion for technology and the ability to learn things quickly is key, as you will be given the opportunity to develop on multiple platforms and in a variety of languages. You will also have the opportunity to participate in discussions with our product and design teams to build the next generation of aboree.</p>
        <h3>Requirements</h3>
        <ul>
            <li>2+ years of software engineering experience.</li>
            <li>The ability to write high quality code efficiently.</li>
            <li>Passion for learning new technologies and the ability to do so quickly.</li>
            <li>Understanding of modern web programming practices.</li>
            <li>Self-motivated and possessing strong communication skills.</li>
            <li>Authorization to work in the United States.</li>
        </ul>
        <h3>Pluses</h3>
        <ul>
            <li>Experience with Flash/ActionScript3.</li>
            <li>Familiarity with video codecs, protocols and wire formats.</li>
            <li>Experience developing iOS and Android.</li>
            <li>Ability to work with small teams.</li>
        </ul>
        <p>To apply, please complete the aboree Coding Challenge. 
            Once you have done so, 
            please submit your resume and your challenge solution to dev_challenge@aboree.com. 
            You may want to include instructions on how to run your solution. 
            Please put the coding challenge answer in the subject line of your email.</p>
    </div>
</div>