<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;

$this->pageclass = 'home-page';
?>
<?php if (Yii::app()->user->isGuest) { ?>
    <div class="front-containter">
        <div class="image-wrapper">
            <video id="guest_video" class="main-image" autoplay loop preload="auto">
                <source src="storage/videos/background.mp4" type='video/mp4' />
                <source src="storage/videos/background.webm" type='video/webm' />
                <source src="storage/videos/background.ogv" type='video/ogg' />
            </video>
        </div>
        <div class="dots">
            <?php include dirname(dirname(__FILE__)).'/static/header.php'; ?>
            <div class="container">
                <div class="home-wrapper">
                    <div class="welcome">
                        <h1 class="welcome-notice">welcome to <?php echo CHtml::encode(Yii::app()->name); ?></h1>
                        <span class="welcome-slogan">...what happened last night?</span>
                    </div>
                    <div class="user-wrapper">
                        <div class="form-block">
                            <!--Login form start-->
                            <?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'login-form',
                                    'enableClientValidation'=>true,
                                    'clientOptions'=>array(
                                            'validateOnSubmit'=>true,
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'form',
                                        'autocomplete'=>'off',
                                    ),
                            )); ?>
                            <div class="row no-margin">
                                <div class="input-field">
                                    <div class="username-field">
                                        <?php echo $form->textField($model,'username', array( 'placeholder'=>'username', 'class'=>'input username', 'autocomplete'=>'off', )); ?>
                                        <?php echo $form->error($model,'username'); ?>
                                    </div>
                                    <div class="password-field">
                                        <?php echo $form->passwordField($model,'password', array('placeholder'=>'password', 'class'=>'input password')); ?>
                                        <?php echo $form->error($model,'password'); ?>
                                    </div>
                                </div>
                                <?php echo CHtml::submitButton('login', array('class'=>'login-button')); ?>
                                <div class="clearfix"></div>
                            </div>
                            <!--<div class="row rememberMe">-->
                                <?php // echo $form->checkBox($model,'rememberMe'); ?>
                                <?php // echo $form->label($model,'rememberMe'); ?>
                                <?php // echo $form->error($model,'rememberMe'); ?>
                            <!--</div>-->
                        <?php $this->endWidget(); ?>
                        <!--Login form end-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
<?php } ?>
