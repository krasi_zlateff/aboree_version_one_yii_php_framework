<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php include dirname(dirname(__FILE__)).'/static/header_assets.php'; ?>
</head>
<body>
    <div class="main-wrapper <?php echo $this->pageclass;?>" id="page">
        <?php include dirname(dirname(__FILE__)).'/static/we_use_cookies.php'; ?>
        <?php echo $content; ?>
        <?php // if (isset($this->breadcrumbs)): ?>
            <?php
//                $this->widget('zii.widgets.CBreadcrumbs', array(
//                    'links' => $this->breadcrumbs,
//                ));
            ?><!-- breadcrumbs -->
        <?php // endif ?>
        <div class="clear"></div>
        <?php include dirname(dirname(__FILE__)).'/static/footer.php'; ?>
    </div><!-- page -->
    <?php include dirname(dirname(__FILE__)).'/static/external_footer_libs.php'; ?>
</body>
</html>
