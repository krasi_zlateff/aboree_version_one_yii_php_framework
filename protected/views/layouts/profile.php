<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/profilepage'); ?>
<div id="content">
    <div class="front-containter">
        <div class="image-wrapper">
            <div class="show-menu-button">
                <span class="label">menu</span>
                <span class="icon open"></span>
            </div>
            <video id="guest_video" class="main-image" autoplay loop preload="auto">
                <source src="storage/videos/loggedin_background.mp4" type='video/mp4' />
                <source src="storage/videos/loggedin_background.webm" type='video/webm' />
                <source src="storage/videos/background.ogv" type='video/ogg' />
            </video>
            <div class="dots">
                <div class="sidebar-wrapper its-hidden">
                    <div class="sidebar">
                        <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" class="logo">
                            <span class="app-name">
                                <?php echo CHtml::encode(Yii::app()->name); ?>
                            </span>
                        </a>
                        <div class="main-menu">
                            <?php
                            $this->widget('zii.widgets.CMenu', array('items' => array(
                                    array('label' => 'home', 'url' => array('/site/index')),
                                    array('label' => 'about', 'url' => array('/site/page', 'view' => 'about')),
                                    array('label' => 'feedback', 'url' => array('/site/contact')),
                                    array('label' => 'help', 'url' => array('/site/page', 'view' => 'help')),
                                    array('label' => 'careers', 'url' => array('/site/page', 'view' => 'careers')),
                                    array('label' => 'privacy', 'url' => array('/site/page', 'view' => 'privacy')),
                                    array('label' => 'cookies', 'url' => array('/site/page', 'view' => 'cookies')),
                                    array('label' => 'login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                    array('label' => 'logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                                ),
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div><!-- content -->
<?php $this->endContent(); ?>