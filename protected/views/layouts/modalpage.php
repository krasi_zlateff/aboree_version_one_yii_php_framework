<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php include dirname(dirname(__FILE__)).'/static/header_assets.php'; ?>
</head>
<body>
    <div class="main-wrapper <?php echo $this->pageclass;?>" id="page">
        <?php include dirname(dirname(__FILE__)).'/static/we_use_cookies.php'; ?>
        <div class="front-containter">
            <div class="image-wrapper">
                <img class="main-image" id="main-image" src=<?php echo $this->headimage?$this->headimage:"/storage/images/backgrounds/7a.jpg";?> />
            </div>
            <div class="dots">
                <?php include dirname(dirname(__FILE__)). '/static/header.php'; ?>
                <div class="container">
                    <div class="modal-wrapper">
                        <?php if ($this->maintitle) { ?>
                            <h1 class="main title">
                                <?php echo $this->maintitle; ?>
                            </h1>
                        <?php } ?>
                        <?php if ($this->subtitle) { ?>
                            <h3 class="sub title">
                                <?php echo $this->subtitle; ?>
                            </h3>
                        <?php } ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="container">
            <?php echo $content; ?>
        </div>
        <div class="clear"></div>
        <?php include dirname(dirname(__FILE__)).'/static/footer.php'; ?>
    </div><!-- page -->
    <?php include dirname(dirname(__FILE__)).'/static/external_footer_libs.php'; ?>
</body>
</html>
