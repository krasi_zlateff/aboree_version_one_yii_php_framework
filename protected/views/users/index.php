<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */

    $this->layout = 'modal';
    $this->pageTitle = Yii::app()->name . ' - register';
    $this->breadcrumbs = array(
        'register',
    );

    $this->pageclass = 'modal-page register-page';
    $this->maintitle = 'open premium account';
    $this->subtitle = 'party night starts over';
    $this->headimage = '/storage/images/backgrounds/6a.jpg';
?>
<p>Please fill out the following form with your login credentials:</p>
<div class="row no-margin">
    <div class="form-block">
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'users-index-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions'=>array(
                    'class'=>'form',
                ),
            ));
        ?>
        <p class="note">Fields below are required *</p>
        <?php echo $form->errorSummary($model); ?>
        <div class="row no-margin">
            <div class="input-field">
                <?php echo $form->textField($model,'username', array( 'placeholder'=>'username', 'class'=>'input username')); ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>
            <div class="input-field">
                <?php echo $form->passwordField($model, 'password', array('placeholder'=>'password', 'class'=>'input password', 'size' => 20, 'maxlength' => 64)); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
            <div class="input-field">
                <?php echo $form->textField($model,'email', array( 'placeholder'=>'email', 'class'=>'input email')); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="clearfix"></div>
        </div>
<!--            <div class="row no-margin">
            <div class="date-of-birth">
                <span class="dob">date of birth</span>
                <select class="month" disabled="disabled">
                    <option>month</option>
                </select>
                <select class="day" disabled="disabled">
                    <option>day</option>
                </select>
                <select class="year" disabled="disabled">
                    <option>year</option>
                </select>
            </div>
        </div>-->

        <div class="row no-margin">
            <div class="input-field">
                <div class="gender">
                    <span class="sex">Gender</span>
                    <div class="compactRadioGroup">
                        <?php echo $form->radioButtonList($model, 'gender', array('Male' => 'Male', 'Female' => 'Female'), array('separator' => ' ')); ?>
                    </div>
                    <?php echo $form->error($model, 'gender'); ?>
                </div>
            </div>
            <div class="input-field">
                <?php if (CCaptcha::checkRequirements()): ?>
                    <div class="captcha">
                        <?php $this->widget('CCaptcha', array('showRefreshButton' => false, 'clickableImage' => true)); ?>
                        <?php echo $form->textField($model, 'verifyCode', array('class'=>'captcha-field')); ?>
                        <div class="clearfix"></div>
                        <?php echo $form->error($model, 'verifyCode'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="input-field">
                <?php echo CHtml::submitButton('open account', array('class'=>'sign-up button')); ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php $this->endWidget(); ?>
        <div class="clearfix"></div>
    </div>
</div>
<div class="row no-margin">
    <div class="login-separator">
        <span>or sign in through</span>
    </div>
</div>
<div class="row no-margin">
    <div class="social-media-login">
        <div class="soundcloud">
            <span class="icon"></span>
            <span class="social-media-login-text">SoundCloud</span>
        </div>
        <div class="facebook">
            <span class="icon"></span>
            <span class="social-media-login-text">facebook</span>
        </div>
        <div class="twitter">
            <span class="icon"></span>
            <span class="social-media-login-text">twitter</span>
        </div>
    </div>
</div>

<div class="agreement">
    If you use "SoundCloud" or "facebook" or "twitter" for opening new premium account and are not an aboree user,<br /> 
    you will be registered and you agree to aboree's 
    <a href="<?php echo $this->createUrl('site/page',array('view'=>'privacy'))?>"><u>privacy policy</u></a>.
</div>
<div class="form-block-footer">
    Already have an account? <a href="<?php echo $this->createUrl('site/login')?>">Login</a> here.
</div>