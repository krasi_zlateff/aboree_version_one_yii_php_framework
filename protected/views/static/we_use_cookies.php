<div class="we-use-cookies">
    <div class="container">
        <div class="cookie-notice">
            <p>We, at aboree use cookies to deliver our services. By using our website, you agree to the use of cookies as described in our <a href="<?php echo $this->createUrl('site/page', array('view'=>'cookies'))?>">Cookie Policy.</a></p>
        </div>
        <div class="close-notice">
            <span class="fi-x"></span>
        </div>
    </div>
</div>