<div id="footer">
    <div class="container">
        <div class="row no-margin">
            <div class="col-lg-10">
                <div class="menu">
                    <?php
                        $this->widget('zii.widgets.CMenu', 
                                array('items' => array(
                                                    array('label' => 'home', 'url' => array('/site/index'), 'itemOptions' => array('class' => 'first-element')),
                                                    array('label' => 'about', 'url' => array('/site/page', 'view' => 'about')),
                                                    array('label' => 'feedback', 'url' => array('/site/contact')),
//                                                    array('label' => 'help', 'url' => array('/site/page', 'view' => 'help')),
                                                    array('label' => 'careers', 'url' => array('/site/page', 'view' => 'careers')),
                                                    array('label' => 'privacy', 'url' => array('/site/page', 'view' => 'privacy')),
                                                    array('label' => 'cookies', 'url' => array('/site/page', 'view' => 'cookies')),
                                                    array('label' => 'login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                                    array('label' => 'logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'itemOptions' => array('class' => 'last-element'),  'visible' => !Yii::app()->user->isGuest),
                                                    array('label' => 'register', 'url' => array('/users/index'), 'itemOptions' => array('class' => 'last-element'), 'visible' => Yii::app()->user->isGuest),
                            ),
                        ));
                    ?>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="logo">
                    <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" class="logo">
                        <span class="app-name">
                            <?php echo CHtml::encode(Yii::app()->name); ?>
                        </span>
                        <span class="version-number">v1.0</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row no-margin">
            <div class="col-lg-12">
                <div class="copyright-and-madein-wrapper">
                    <div class="copyright">
                        <?php echo CHtml::encode(Yii::app()->name); ?> 
                        <span class="stamp">&copy;</span> 2012 - <?php echo date('Y');?>. 
                        all rights reserved.
                    </div><div class="clearfix"></div>
                    <div class="made-in-sofia">
                        made with <span class="fi-heart size-16 love"></span> in sofia
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>