<div class="header">
    <div class="container">
        <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" class="logo <?php echo Yii::app()->user->isGuest?"not-logged":"logged";?>">
            <span class="app-name">
                <?php echo CHtml::encode(Yii::app()->name); ?>
            </span>
        </a>
        <?php if (Yii::app()->user->isGuest) { ?>
            <div class="soundcloud"></div>
            <a href="<?php echo $this->createUrl('users/index')?>" class="open-account">
                <span class="icon"></span>Join
            </a>
        <?php } else { ?>
            <div class="user-nav">
                <div class="user profile">
                    <div class="profile-image">
                        <img src="storage/images/avatars/avatar.png">
                    </div>
                    <span class="name">
                        <?php echo CHtml::encode(Yii::app()->user->name); ?>
                    </span>
                </div>
                <div class="user messages">
                    <div class="icon fi-mail"></div>
                </div>
                <div class="user settings">
                    <div class="icon fi-widget"></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>