<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'aboree',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// comment the following to disable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'asdasd',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','192.168.0.*'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
                'clientScript'=>array(
                    'packages'=>array(
                        'jquery'=>array(
                            'baseUrl'=>'//ajax.googleapis.com/ajax/libs/jquery/1/',
                            'js'=>array('jquery.min.js'),
                        ),
                        'jquery-cookie'=>array(
                            'baseUrl'=> '/javascripts/lib/jquery-cookie/',
                            'js'=>array('jquery.cookie.js'),
                        ),
                        'jquery-easing'=>array(
                            'baseUrl'=>'/javascripts/lib/jquery-easing/',
                            'js'=>array('jquery.easing.1.3.js', 'jquery.easing.compatibility.js'),
                        ),
                        'soundcloud'=>array(
                            'baseUrl'=>'http://connect.soundcloud.com/sdk/',
                            'js'=>array('sdk-3.0.0.js'),
                        ),
                    ),
                ),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
                /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		), */
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=aboree',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'asdasd',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
                    'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters'=>array('127.0.0.1','192.168.0.*'),
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
                                /*
				array(
					'class'=>'CWebLogRoute',
				),
                                */
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'admin@aboree.dev',
	),
);