$(document).ready(function(){
    $('.close-notice').on("click", function(){
        $('.we-use-cookies').slideUp({duration:"5000", easing: "easeInOutBack"});
    $.cookie('cookie-name', 'we-use-cookie', { expires: 7, path: '/' });
    });
    
    if ($.cookie('cookie-name') === 'we-use-cookie') {
        $('.we-use-cookies').addClass('hide');
    }
    
    $('.show-menu-button').on('click', function(e){
        e.preventDefault();        
        var target = $('.sidebar-wrapper');
        var parent = $(this);
        var menu_icon = parent.find('.icon');
                
        if (menu_icon.hasClass('open')) {
            menu_icon.removeClass('open').addClass('close-menu fi-x');
        } else {
            menu_icon.removeClass('close-menu fi-x').addClass('open');
        }
        
        if (target.hasClass('its-hidden')) {
            target.removeClass('its-hidden');
            target.addClass('its-visible');
        } else {
            target.removeClass('its-visible');
            target.addClass('its-hidden');
        }
    });
    
    //Register page on click join button, scrollTo register form.
    $('.register-page .header .open-account').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#content").offset().top
        }, 800);
    });
       
    // SoundCloud
    // initialize client with app credentials
    SC.initialize({
        client_id: 'bdae6a6b9a7a4dc9f8ee20932166121b',
        redirect_uri: 'http://aboree.com/callback.html'
    });
    
    
    //TODO: Get it works!
    $('.soundcloud').on('click', function (e) {
        e.preventDefault();
        // initiate auth popup
        SC.connect().then(function () {
            return SC.get('/me');
        }).then(function (me) {
            alert('Hello, ' + me.username);
        });
    });
});